//importa el plugin 'path' de node, permite unir directorios
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const miniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
//    archivo de entrada
    entry: './src/app.js',
    mode: "production", //define el entorno donde se esta trabajando, dependiendo de esta configuracion muestra el codigo minificado o no
//    ruta donde se ubicara el archivo de salido y nombre del archivo
    output: {
        path: path.resolve(__dirname, '../dist'),
    //    __dirname hace referencia a la carpeta del proyecto actual
    //    dist es el nombre de la carpeta donde se guardara la slaida
        filename: 'bundle.js',
    },
    module: {
        rules: [
            {
                test: /\.(sa|sc|c)ss$/, //pruebe todos los archivos .css .sass .scss
                use: [
                    miniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader',
                ], // utilizando estos modulos
            },
            { //configura la carga de imagenes - es necesario que estas imagenes se importen en el .js
                test: /\.(jpg|png|svg|jpeg|gif)$/, //la $ significa  que esten al final
                use:[
                    {
                        loader: 'file-loader',
                        options: {
                            name:'[name].[ext]',
                            outputPath: 'static/',
                            useRelativePath: true
                        }
                    }
                ]
            }

        ]
    },
//    los plugin permiten extender las funcionalidades cuando se trabaja con webpack
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.handlebars',
            //configuracion para minificar el html
            minify: {
                collapseWhitespace: true,
                keepClosingSlash: true,
                removeComments: true,
                removeRedundantAttributes: true,
                removeScriptTypeAttributes: true,
                removeStyleLinkTypeAttributes: true,
                useShortDoctype: true
            }
        }),
        new miniCssExtractPlugin({
            filename: 'main.css' //por defecto lo guarda en dist, se le indica crear un archivo main.scss con la salida

        })
    ]
}